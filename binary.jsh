String toBinary(int number){ /*funkcja do zamiany liczb z systemu dziesietnego do binarnego*/
    int i=0; //zmienna do petli
    int[] binary = new int[4]; //tablica do zapisania wyniku
    if(number<0||number>15){
        return "Error"; // jesli podana liczba ma wiecej niz 4 bity
    }else{
        while(i<4){ // przeliczanie na system binarny
            if(number%2==0){ 
                binary[i]=0;
                number/=2;
            }else{
                binary[i]=1;
                number/=2;
            }
            i++;
        }
        String result = ""; // zmienna do return
        for(i=3;i>=0;i--){
            result+=binary[i];
        }
        return result; // zwrot wyniku
    }
}
int number = 6; // testowa liczba

System.out.println(toBinary(number)); // wyswietlenie wyniku na ekran
/exit